angular.module('seartipy').factory 'Products', ->
  products = _.map _.range(23), (id) ->
    id: id
    name: Faker.Lorem.words(2).join ' '
    price: Faker.random.number 500
    description: Faker.Lorem.sentence()

  get: (id) -> products[id]
  index: -> products
  update: (p) -> _.assign((get p.id), p)
  add: (p) -> products.push _.assign { id: products.length },
    _.pick p, ['name', 'price', 'description']
