(angular.module 'seartipy').controller 'ShoppingController', ($scope, Products, Cart) ->

  $scope.products = Products.index()
  $scope.cart = -> Cart.index()

  $scope.pageSize = 8
  $scope.pageSelected = 0

  $scope.maxPage = (filteredProducts)->
    if filteredProducts == undefined or filteredProducts.length < $scope.length
      return 1
    parseInt( (filteredProducts.length + $scope.pageSize - 1)/ $scope.pageSize, 10 )

  isPageValid  = (filteredProducts, page) ->
     page >= 0 && page < $scope.maxPage(filteredProducts)

  $scope.changePage = (filteredProducts, d) ->
    if isPageValid(filteredProducts, $scope.pageSelected + d)
      $scope.pageSelected += d

  $scope.removeFromCart = Cart.remove
  $scope.bought = Cart.contains
  $scope.buy = Cart.addOrUpdate

  $scope.$watch 'searchTerm', -> $scope.pageSeleted = 0
